const { Schema, model } = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const commentSchema = new Schema({
    idTrabajador: {
        type: Schema.Types.ObjectId,
        required: true,
        trim: true,
    },
    idEmpleador: {
        type: Schema.Types.ObjectId,
        required: true,
        trim: true
    },
    idServicio: {
        type: Number,
        required: true,
        trim: true,
    },
    comentario: {
        type: String,
        required: true,
        trim: true
    },
    calificacion: {
        type: Number,
        required: true
    }
}, {
    timestamps: true
});
commentSchema.plugin(uniqueValidator, { message: '{PATH} debe de ser único' });

module.exports = model('Comment', commentSchema);