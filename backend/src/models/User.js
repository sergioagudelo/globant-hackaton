const { Schema, model } = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
// const rolesValidos = {
//     values: ['ADMIN_ROLE', 'USER_ROLE'],
//     message: '{VALUE} no es un rol valido'
// };

const userSchema = new Schema({
    // rol: {
    //     type: String,
    //     default: 'USER_ROL',
    //     enum: rolesValidos,
    // },
    nombre: {
        type: String,
        required: true,
        trim: true
    },
    email: {
        type: String,
        required: true,
        unique: true,
        trim: true
    },
    numeroCelular: {
        type: String,
        required: true,
        unique: true,
        trim: true
    },
    fotoPerfil: {
        type: String,
        trim: true
    },
    contrasena: {
        type: String,
        required: true,
        trim: true
    },
    edad: {
        type: Number,
        required: true,
        trim: true
    },
    servicios: {
        type: Array,
        trim: true
    }
}, {
    timestamps: true
});

userSchema.plugin(uniqueValidator, { message: '{PATH} debe de ser único' });
module.exports = model('User', userSchema);


// Services data structure
// [
//     {
//         id: number, // aumente
//         servicio: String,
//         estado: Boolean,
//         categoria: String,
//         precio: Number,
//         aConvernir: Boolean,
//         incluye: String,
//         noIncluye: String,
//         descripcionCosto: String,
//         experiencia: String,
//         ciudad: String,
//         ubicacion: Array[latitud, longitud],
//         horario: String,
//         fotos: Array[URLs],
//         youtube: String,
//         facebook: String,
//         instagram: String,
//         calificacionPromedio: Number,
//     }
// ]