const { Router } = require('express');
const router = Router();

const twilioCtrl = require('../libs/twilio/twilio.js');

router.post('/sendSms', [], twilioCtrl.sendSms);

module.exports = router;