const { Router } = require('express');
const router = Router();

const nodemailerCtrl = require('../controllers/email.controller')

router.post('/sendContactServiceEmail', [], nodemailerCtrl.sendContactServiceEmail);

module.exports = router;