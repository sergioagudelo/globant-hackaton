const { Router } = require('express');
const router = Router();

const comment = require('../controllers/comment.controller');

router.get('/getComments', [], comment.getComments);
router.post('/createComment', [], comment.createComment);
router.get('/getComment/:id', [], comment.getComment);
router.get('/getCommentIdTrabajador/:id', [], comment.getCommentIdTrabajador);
router.put('/updateComment', [], comment.updateComment);
router.delete('/deleteComment/:id', [], comment.deleteComment);

module.exports = router;