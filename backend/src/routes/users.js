const { Router } = require('express');
const router = Router();

// Deconstruct
const { getUsers,createUser, deleteUser } = require('../controllers/users.controller');
const user = require('../controllers/users.controller');

router.get('/getUsers', [], user.getUsers);
router.post('/createUser', [], user.createUser);
router.get('/getUser/:id', [], user.getUser);
router.delete('/deleteUser/:id', [], user.deleteUser);

module.exports = router;