// Used to work with nodemailer to send emails
const configMensaje = require('../libs/nodemailer/nodemailer');
const nodemailerCtrl = {};

// email sended from contacto component
nodemailerCtrl.sendContactServiceEmail1 = async (req, res, next) => {
    configMensaje(req.body, 'contact');
    res.status(200).send();
};

// email sended from contacto recovery password
nodemailerCtrl.sendContactServiceEmail = async (req, res, next) => {
    configMensaje(req.body, 'resetPassword');
    res.status(200).send();
};


module.exports = nodemailerCtrl;