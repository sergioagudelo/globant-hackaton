const userCtrl = {};

const User = require('../models/User');

// Status codes
// 200, 201, 400, 403, 404, 500

userCtrl.getUsers = async(req, res) => {
    try {
        const _users = await User.find();
        if (!_users) {
            res.status(400).json({
                ok: false,
                message: 'No existen usuarios registrados en la aplicación.',
            });
        }
        //TODO: res acts as a return? breaks the method?
        res.status(200).json({
            ok: true,
            users: _users,
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            error: 'Error interno del servidor, no se pudó realizar esta operación.',
            error,
        });
    }
};

userCtrl.createUser = async(req, res) => {
    const { nombre, email, numeroCelular, fotoPerfil, contrasena, edad, servicios } = req.body;

    const newUser = new User({
        nombre,
        email,
        numeroCelular,
        fotoPerfil,
        contrasena,
        edad,
        servicios,
    });
    try {
        const _user = await newUser.save();
        if (!_user) {
            res.status(400).json({
                ok: false,
                message: 'No se pudo crear el usuario.',
            });
        }
        res.status(201).json({
            ok: true,
            message: 'Usuario creado.',
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            message: 'Error interno del servidor, no se pudó realizar esta operación.',
            error,
        });
    }
};

userCtrl.getUser = async (req, res) => {
    const { id } = req.params;

    try {
        const _user = await User.findById(id);
        if (!_user) {
            res.status(400).json({
                ok: false,
                message: 'No se ha encontrado el usuario en la aplicación',
            });
        }
        res.status(200).json({
            ok: true,
            user: _user,
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            message: 'Error interno del servidor, no se pudó realizar esta operación.',
            error,
        });
    }
}

userCtrl.deleteUser = async(req, res) => {
    const { id } = req.params;

    try {
        const _user = await User.findByIdAndDelete(id);
        if (!_user) {
            res.status(400).json({
                ok: false,
                message: 'No se pudo eliminar el usuario.',
            });
        }
        res.status(200).json({
            ok: true,
            message: 'User deleted',
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            message: 'Error interno del servidor, no se pudó realizar esta operación.',
            error,
        });
    }
}

module.exports = userCtrl;