const commentCtrl = {};
const mongoose = require('mongoose');
const Comment = require('../models/Comment');
const { SchemaType } = require('mongoose');

commentCtrl.getComments = async(req, res) => {
    try {
        const _comment = await Comment.find();
        if (!_comment) {
            res.status(400).json({
                ok: false,
                message: 'No se han encontrado comentarios en la aplicación.',
            });
        }
        res.status(200).json({
            ok: true,
            comments: _comment,
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            message: 'Error interno del servidor, no se pudó realizar esta operación.',
            error,
        });
    }
};

commentCtrl.createComment = async(req, res) => {
    const { idTrabajador, idEmpleador, idServicio, comentario, calificacion } = req.body;

    const newComment = new Comment({
        idTrabajador,
        idEmpleador,
        idServicio,
        comentario,
        calificacion,
    });
    try {
        const _comment = await newComment.save();
        if (!_comment) {
            res.status(400).json({
                ok: true,
                message: 'No se pudo realizar el comentario.',
            });
        }
        res.status(201).json({
            ok: true,
            message: 'Comentario realizado.',
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            message: 'Error interno del servidor, no se pudó realizar esta operación.',
            error,
        });
    }
};

// 200
// 201
// 400
// 403
// 404
// 500
commentCtrl.getCommentIdTrabajador = async(req, res) => {
    const { id } = req.params;
    const objectId = mongoose.Types.ObjectId(id);
    try {
        const _comment = await Comment.aggregate([
            { $match: { idTrabajador: objectId } },
        ]);
        if (!_comment) {
            res.status(400).json({
                ok: false,
                message: 'No se ha encontrado el comentario en la aplicación.',
            });
        }
        res.status(200).json({
            ok: true,
            comment: _comment,
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            message: 'Error interno del servidor, no se pudó realizar esta operación.',
            error,
        });
    }
}

commentCtrl.getComment = async(req, res) => {
    const { id } = req.params;

    try {
        const _comment = await Comment.findOne({ _id: id });
        if (!_comment) {
            res.status(400).json({
                ok: false,
                message: 'No se ha encontrado el comentario en la aplicación.',
            });
        }
        res.status(200).json({
            ok: true,
            comment: _comment,
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            message: 'Error interno del servidor, no se pudó realizar esta operación.',
            error,
        });
    }

}

commentCtrl.deleteComment = async(req, res) => {
    const { id } = req.params.id;

    try {
        const _comment = await Comment.findByIdAndDelete(id);
        if (!_comment) {
            res.status(400).json({
                ok: false,
                message: 'No se ha encontrado el comentario en la aplicación.',
            });
        }
        res.status(200).json({
            ok: true,
            message: 'Comentario eliminado correctamente.',
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            message: 'Error interno del servidor, no se pudó realizar esta operación.',
            error,
        });
    }
}

commentCtrl.updateComment = async(req, res) => {
    const { idComentario, idTrabajador, idEmpleador, comentario, calificacion } = req.body;
    try {
        const _comment = await Comment.findByIdAndUpdate(idComentario, {
            comentario,
            calificacion,
        }, { new: true });

        if (!_comment) {
            res.status(400).json({
                ok: false,
                message: 'No se pudo actualizar el comentario en la aplicación.',
            });
        }
        res.status(200).json({
            ok: true,
            message: 'Comentario actualizado',
            _comment,
        });
    } catch (error) {
        res.status(500).json({
            ok: false,
            message: 'Error interno del servidor, no se pudó realizar esta operación.',
            error,
        });
    }

}

module.exports = commentCtrl;