require('../../config.js');
const nodemailer = require('nodemailer');

module.exports = (formulario, useCase) => {
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: process.env.EMAIL_USER,
            pass: process.env.EMAIL_PASSWORD,
        }
    });

    var urImgBookEat = 'https://bookeat1.s3.eu-west-1.amazonaws.com/BookEat.png?response-content-disposition=inline&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEAIaCXVzLWVhc3QtMSJGMEQCIEs%2F9fSpOSdZtu0zjFn%2FB%2FVXvZ2hdslcrN17%2FyXYoaS2AiBNCYeHVgC89nbgA%2BsiM378DTH2L%2Bo9bepLY94MVi1TzSqWAgib%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F8BEAEaDDY4OTUyOTU5MTIwNyIMYNr9ZleuMzGulelWKuoB1Uat8zKoa1%2F95l74g%2Bq8f56yXOmMcE8WPHCncBjp%2FNps3IHma2GzcVK1ahaxLT1w5UKTiERLGG3XoLkLq73bUjx%2B1KtuwDjCUUhcMbSjozuzpWmDCujEaroNkm6DXtDJQrdyfN1TQJLm20HplUIK%2B%2FFyjomad213odhAMwNePDYKD2%2Bt2QrvoooGdEqU1g%2FTahumIGhgxAHJDSyw8HzjlhsuvTf%2FebsHDKddMRA6eeEZfLaIqTVp58d83HQBeb%2B%2B7syLtDZjrQvDDC7Xsm0iAFBTgK44UWArqhAIExn2%2Fgfs%2B5%2BiCIKMY3KfMJ3%2BuPEFOoMDXEMdMLMJWIZLoGHz9H8nh6LeoJxv7YR3tqtgyDxZEs9%2BD5%2BYzzB6DvoMiIecBUF%2BILLyAwY4jYnkfj7NN0GCN0P8bfF9DGn2F4M72Y7vnIbrVv487m4oQJd8AKFYl4MlVDiYV4MxkY8LprPBiNVHHwYbwAhfSvD2nMirJUXEshObGoB6Z8jPEcKZCftKYMY3INNCasr8N6Bv3UmWnvxDs8%2Ff8mi%2B0F2fZ8d2JalEjJdW61HQEXn4uGdz%2F%2Fznxf6w7wDifttcgKRBGB8u63rbJIErhKhcUi4yE0PvbEhr8jHbwRmI%2FX5cQXzyRUT2u%2BG1oWenFwqnwiHOg4kcSjjrVJIud5EaFj17luQtm8sxQzzXLxVhGfIxhJxHAQ0DyXyS50Ud2%2ByFarZ8m%2Fov0cp4IZesSsyRQDSPlBWCIiFp7v9m9WMd2xrOw6V%2BPqbvtHLkbHq8sb9pHtNR5n3sojsWSsiAAsoap0IHWi%2Bx1VU%2FIU%2BvNdKGB2khV1ABAL7%2Fr%2BJSO4ge&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20200127T013847Z&X-Amz-SignedHeaders=host&X-Amz-Expires=300&X-Amz-Credential=ASIA2BCZKSWT6FWJP4ME%2F20200127%2Feu-west-1%2Fs3%2Faws4_request&X-Amz-Signature=bfbcfed7adb17343a10d810ad9b4eb349114d9670f01d32251b03b3921a45b1e';
    var bodyBackground = 'https://bookeat1.s3.eu-west-1.amazonaws.com/header-bg.jpg?response-content-disposition=inline&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEOf%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCXVzLWVhc3QtMSJGMEQCIGG2RMNEnu%2FoPPn7x3sbst8RZrAYHrKT0AOU2Kr%2BDsamAiAUrgcuweBcJzkF1Jsv6qtpoHflO05z%2F7r%2BjZ7afapO4iqWAgiA%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F8BEAEaDDY4OTUyOTU5MTIwNyIMCvgeNtKAxB1u6kIPKuoBN87OIxTDeBh7cVLqlxtHv5RORfHe4Lnl1JqXs00E3hdeX1fA3pTQv2am289Mdd2poPADWfpySGq14YikX0oCVCMKQYpEYLgnFoTad2shzchQ656zlrlZVpF8UUh2F3eT0P0KGHkSNla%2BLpTEzyeIhQ3g%2FcSRWSkVRzuejYIlJdSH%2BstwXtp2arBPg6xvX6JciCJoW0OfbFJyGzXOhKm73LM0X1SjIgF7rSrY6J8V5alvVEvAtQhNRfCL%2FLSBPnuFAvfxoiES7EJrNEGJJ%2BXo68WQ2qQ8vzt4Q3ljcefscVs0Phu473xRp1UgMK%2BQs%2FEFOoMDp6ZZeqseJkrkF08XKYsfWQ5sqX6ytS5VwAyJl98BGfrn0nL9kaedkQxd%2Fg4Ff827dEgNflW3EJtOJJvlXypBajtyFzhdND4gfbkDlpDoNb5tR2yOuginfyfug0pfgwUMwVV8TUKb6SqbchTLuolhZjkX6XnxVwTugubhqYqYNcBZQrtnZmAV0mUQkbgYqAYcz0ZpMmIY18jiLCTAZMPgKDKzHq%2BwB5mdV3LRKnlPG%2Fcce8rRzW9eBfHxV7Gufb964vGm2pulNbY8k9Di0cGRGRPh2dEjn1oxJiQlTjoYiqSKFhfPydyovlrV7hQIFTB4Cz8qj9KkUNJC8x3dMzadTdtBbCt5cgv33Br%2B0d7jTDBRx4CJeJxMR1brrop9NAZksyuK6QO2QavrmwyTl%2F1Bix0I2jOljMOSJMBtWumgIAmByotMsxRwh%2BprX4yByLZVmUApr3R7CLCjLz8XugPsydnLONUTp4FxRyOyHYn5g6Id8WfQJiXsdHEucwbUovY3iZ%2Fk&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20200125T232434Z&X-Amz-SignedHeaders=host&X-Amz-Expires=300&X-Amz-Credential=ASIA2BCZKSWTQMTUBNPQ%2F20200125%2Feu-west-1%2Fs3%2Faws4_request&X-Amz-Signature=4d230f74d7933a9cd5aa4e2ccad2f753340c609a2fde415fc8f382f59bdb3ca3';
    var to = '';
    var content = '';
    var subject = '';
    var title = '';

    if(useCase === 'contact'){
        to = 'bookeat2019@gmail.com';
        subject = 'Contacto desde ' + formulario.nombreRestaurante;
        title = 'Un nuevo restaurante quiere contactar con BookEat!';
            // <strong>Nombre contacto:</strong> ${formulario.nombre} <br/>
            // <strong>Nombre del restaurante:</strong> ${formulario.nombreRestaurante} <br/>
            // <strong>Direccion del restaurante:</strong> ${formulario.direccion} <br/>
            // <strong>E-mail contacto restaurante:</strong> ${formulario.email} <br/>
            // <strong>Telefono de contacto restaurante:</strong> ${formulario.telefonoContacto} <br/>
            // <strong>Mensaje:</strong> ${formulario.mensaje}
            content = `
            <h1>Hola team BookEat</h1>
            <p class="title">Un restaurante quiere contactar con el team mas powerfull!</p>
            <p>Nombre del restaurante: <strong>${formulario.nombreRestaurante}</strong>.</p>
            <p>Direccion del restaurante: <strong>${formulario.direccion}</strong>.</p>
            <p>Nombre de la persona contactando: <strong>${formulario.nombre}</strong>.</p>
            <p>E-mail de contacto del restaurante: ${formulario.email}.</p>
            <p>Telefono de contacto restaurante: <strong>${formulario.telefonoContacto}</strong>.</p>
            <p>Mensaje: <br><strong>${formulario.mensaje}</strong>.</p><br>
            <p><strong>Gracias team <strong>BookEat</strong> por hacer de este proyecto posible.</strong></p><br>
            <a href="https://bookeat-app.herokuapp.com/view-admin" class="button">Ir a BookEat Admin</a>
        `;
    }else if(useCase === 'resetPassword'){
        to = formulario.email
        subject = 'Restablecer contraseña BookEat';
        title = 'Saludos desde BookEat';
        content = `
            <h1>Hola ${formulario.nombre}</h1>
            <p class="title">Te escribimos desde BookEat para confirmar tu proceso de olvido de contraseña.</p>
            <p>Tu nueva contraseña es: <strong>${formulario.newPassword}</strong>.</p>
            <p>Por seguridad te pedimos que la cambies cuando antes.</p>
            <p><strong>Gracias por preferirnos para encontrar sitios mágicos para comer.</strong></p>            
            <a href="https://bookeat-app.herokuapp.com/" class="button">Ir a BookEat</a>
        `;
    }

    var html = `
    <!DOCTYPE html>
    <html>
        <head>
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
            <style>
                .card {
                    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
                    max-width: 300px;
                    margin: auto;
                    text-align: center;
                    font-family: arial;
                }
                
                .title {
                    color: grey;
                    font-size: 18px;
                }
                
                a {
                    text-decoration: none;
                    font-size: 22px;
                    color: black;
                    }
                    
                a.button:hover, a:hover {
                    opacity: 0.7;
                }

                a.button {
                    -webkit-appearance: button;
                    -moz-appearance: button;
                    appearance: button;
                
                    text-decoration: none;
                    color: white;

                    background-color: #FF2E2E;
                    border-radius: 4px;
                    padding: 5px;
                }
            </style>
        </head>
        <body>
            <h2 style="text-align:center">${title}</h2>
            <div class="card">
                <img src="${urImgBookEat}" alt="BookEat" style="width:100%">
                ${content}
            </div>
        </body>
    </html>
    `;

    const mailOptions = {
        // from: formulario.email,
        to: to,
        subject: subject,
        html: html
    };
    transporter.sendMail(mailOptions, function (err, info) {
        if (err)
            console.log(err)
        else
            console.log(info);
    });
}