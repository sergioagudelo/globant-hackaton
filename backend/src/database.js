require('./config.js');
const mongoose = require('mongoose');

console.log(process.env.URL_DB)
mongoose.connect(process.env.URL_DB, {
    useNewUrlParser: true,
    useCreateIndex: true
})
.then(db => console.log('DB is connected.', process.env.URL_DB) )
.catch(err => console.log(err));

const connection = mongoose.connection;

// Once the DB is connected a log message is shown
// connection.once('open', () => {
//     console.log('Database is connected');
// });