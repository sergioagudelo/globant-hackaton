if (process.env.NODE_ENV !== 'production'){
    require('dotenv').config();
}

// Enviroment
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

// Database
let urlDB;
if (process.env.NODE_ENV === 'development') {
    urlDB = 'mongodb://localhost/globhackdb';
} else {
    urlDB = process.env.MONGODB_URI;
}
process.env.URL_DB = urlDB;

// Port
process.env.PORT = process.env.PORT || 3000;

// Twilio KEYs
process.env.TWILIO_ACCOUNT_SID = process.env.TWILIO_ACCOUNT_SID || undefined;
process.env.TWILIO_AUTH_TOKEN = process.env.TWILIO_AUTH_TOKEN || undefined;
process.env.TWILIO_NUMBER = process.env.TWILIO_NUMBER || undefined;
process.env.EMAIL_USER = process.env.EMAIL_USER || undefined;
process.env.EMAIL_PASSWORD = process.env.EMAIL_PASSWORD || undefined;



// "postinstall": "npm run build",
// "build": "cd frontend && npm run build"