require('./config.js');
const express = require('express');
const cors = require('cors');
const morgan = require('morgan');

const app = express();

// settings
app.set('port', process.env.PORT);

// middlewares 
app.use(cors({ origin: [process.env.FRONT_URI, 'http://localhost:4200'] }));
app.use(express.json());
app.use(morgan('dev'));

// routes
app.use('/api/comments', require('./routes/comment'));
app.use('/api/users', require('./routes/users'));
app.use('/sms', require('./routes/sms'));
app.use('/email', require('./routes/email'));

module.exports = app;